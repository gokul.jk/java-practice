package com.cbt.democrud;

import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import com.cbt.democrud.dao.StudentDAO;
import com.cbt.democrud.entity.Student;

@SpringBootApplication
public class DemocrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemocrudApplication.class, args);
	}
	
	@Bean
	public CommandLineRunner commandlinerunner(StudentDAO studentDAO) {
		return runner ->{
		   //createstudent(studentDAO);
//		   createmultiplestudent(studentDAO);
//		   Findstudent(studentDAO);
//		   FindAllstudent(studentDAO);
//		   updatestudent(studentDAO);
		   deletestudent(studentDAO);
	};
	}

	private void deletestudent(StudentDAO studentDAO) {
		int studentId=2;
		studentDAO.delete(studentId);
		
	}

	private void updatestudent(StudentDAO studentDAO) {
		
		int studentId=2;
		Student myStudent=studentDAO.findById(studentId);
		myStudent.setFirstName("Scooby");
		System.out.println(myStudent);
		
	}

	private void FindAllstudent(StudentDAO studentDAO) {
		List<Student> theStudent=studentDAO.findAll();
		for(Student tempStudent:theStudent) {
			System.out.println(tempStudent);
		}
		
	}

	private void Findstudent(StudentDAO studentDAO) {
		int studentId=3;
		Student myStudent=studentDAO.findById(studentId);
		System.out.println(myStudent);
		
	}

	private void createmultiplestudent(StudentDAO studentDAO) {
		// TODO Auto-generated method stub
		Student tempStudent1=new Student("guna","guna","guna@cbt.com");
		Student tempStudent2=new Student("sam","periya","sam@cbt.com");
		Student tempStudent3=new Student("Arun","pandiyan","arunpandiyan@cbt.com");
		
		studentDAO.save(tempStudent1);
		studentDAO.save(tempStudent2);
		studentDAO.save(tempStudent3);

		
		

	}

	private void createstudent(StudentDAO studentDAO){
		System.out.println("ji");
		Student tempStudent=new Student("Gokul","JK","gokul@cbt.com");
		System.out.println("ji");
		studentDAO.save(tempStudent);
         
}
}
