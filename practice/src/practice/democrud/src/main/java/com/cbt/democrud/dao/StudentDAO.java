package com.cbt.democrud.dao;

import java.util.List;

import com.cbt.democrud.entity.Student;

public interface StudentDAO {
	void save(Student theStudent);
	Student findById(int id);
	List<Student> findAll();
	void update(Student theStudent);
	void delete(int id);

}
