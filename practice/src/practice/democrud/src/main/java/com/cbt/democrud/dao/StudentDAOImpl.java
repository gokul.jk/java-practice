package com.cbt.democrud.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cbt.democrud.entity.Student;

import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;

@Repository
public class StudentDAOImpl implements StudentDAO{
	
	@Autowired
	private EntityManager entityManager;

	@Override
	@Transactional
	public void save(Student theStudent) {
		entityManager.persist(theStudent);
		
	}

	@Override
	public Student findById(int id) {
		// TODO Auto-generated method stub
		
		return entityManager.find(Student.class,id);
	}

	@Override
	public List<Student> findAll() {
		TypedQuery<Student> theQuery=entityManager.createQuery("From Student",Student.class);
		return theQuery.getResultList();
	}

	@Override
	@Transactional
	public void update(Student theStudent) {
		
		entityManager.merge(theStudent);
		
	}

	@Override
	@Transactional
	public void delete(int id) {
		Student theStudent=entityManager.find(Student.class,id);
		entityManager.remove(theStudent);
		
	}

}
