package com.example.form;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class formController {
@GetMapping("student")
public String student() {
	return "stud";
}
@PostMapping("/details")
public String viewdetails(@RequestParam("sid")int sid,
		@RequestParam("sname")String sname,
		@RequestParam("smail")String smail, ModelMap modelmap) 
{	
modelmap.put("sid", sid);
modelmap.put("sname", sname);
modelmap.put("smail", smail);
return "viewstudents";
}
}