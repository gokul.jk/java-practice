package practice;

class A3 {
	int a=10;
	void S() {
		System.out.println("parent class");
			}
}
class B3 extends A3{
	int b=20;
	void S1() {
		System.out.println("child1 class");
	}
}
class C3 extends A3 {
	int c=30;
	void S2() {
		System.out.println("Child2 class");
	}
}

public class hierarchy {
	public static void main(String args[]) {
		C3 obj=new C3();
		System.out.println(obj.c);
		obj.S2();
		System.out.println(obj.a);
		obj.S();
		B3 obj1=new B3();
		System.out.println(obj1.b);
		obj1.S1();
		System.out.println(obj1.a);
		obj1.S();
		
	}
}