package com.example.jsplink.jsplink;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JsplinkApplication {

	public static void main(String[] args) {
		SpringApplication.run(JsplinkApplication.class, args);
	}

}
