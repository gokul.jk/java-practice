package practice;
class A2 {
	int a=10;
	void S() {
		System.out.println("parent class");
			}
}
class B2 extends A2{
	int b=20;
	void S1() {
		System.out.println("child class");
	}
}
class C2 extends B2 {
	int c=30;
	void S2() {
		System.out.println("grandChild class");
	}
}

public class multipleInheritance {
	public static void main(String args[]) {
		C2 obj=new C2();
		System.out.println(obj.c);
		obj.S2();
		System.out.println(obj.b);
		obj.S1();
		System.out.println(obj.a);
		obj.S();
		
	}
}