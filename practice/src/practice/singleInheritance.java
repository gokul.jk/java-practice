package practice;

class A1 {
	int a=10;
	void S() {
		System.out.println("parent class");
			}
}
class B extends A1{
	int b=20;
	void S1() {
		System.out.println("child class");
	}
}
public class singleInheritance {
	public static void main(String args[]) {
		B obj=new B();
		System.out.println(obj.b);
		obj.S1();
		System.out.println(obj.a);
		obj.S();
		
		
	}
}