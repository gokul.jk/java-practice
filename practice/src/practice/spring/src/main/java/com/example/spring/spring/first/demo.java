package com.example.spring.spring.first;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
public class demo {
	@GetMapping("/")
	public String demo2() {
		return "hello";
	}
	@GetMapping("/next")
	public String demo3() {
		return "bye";
	}
	
	@Value("${bike}")
	private String bike;
	
	@GetMapping("/ym")
	public String getValue() {
		return "My fav bike is"+" "+bike;
	}
	
}
