package practice;

class A4 {
	int a=10;
	void S() {
		System.out.println("parent class");
			}
}
class B4 extends A4{
	int a=20;
	void S() {
		System.out.println("child class");
		System.out.println(super.a);
		super.S();
	}
}
public class superclass{
	public static void main(String args[]) {
		B4 obj=new B4();
//		System.out.println(obj.a);
		obj.S();
	
	}
}
