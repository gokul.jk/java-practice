package com.example.first.first;

import org.springframework.stereotype.Component;

@Component
public class example {
	private int studid;
	private String studname;

	public int getStudid() {
		return studid;
	}

	public void setStudid(int studid) {
		this.studid = studid;
	}

	public String getStudname() {
		return studname;
	}

	public void setStudname(String studname) {
		this.studname = studname;
	}

	public void student() {
		System.out.println("student object");
	}
}
